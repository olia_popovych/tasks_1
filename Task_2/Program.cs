﻿using System;

namespace Task_2
{
    class Program
    {
        static void Main(string[] args)
        {
            Point point1 = new Point(1, 1);
            Point point2 = new Point(2, 3);
            Rectangle rec1 = new Rectangle(2, 3, point1);
            Rectangle rec2 = new Rectangle(2, 3, point2);

            Console.WriteLine("\nЗнаходження перетину i об*єднання для таких прямокутникiв:");
            rec1.ShowRectangle();
            rec2.ShowRectangle();
            Rectangle intersec = Rectangle.FindIntersection(rec1, rec2);
            Console.WriteLine("\nПеретин:");
            intersec.ShowRectangle();

            Rectangle union = Rectangle.FindUnion(rec1, rec2);
            Console.WriteLine("\nОб*єдняння");
            union.ShowRectangle();

            Point point3 = new Point(1, 1);
            Point point4 = new Point(4, 5);

            rec1 = new Rectangle(2, 3, point3);
            rec2 = new Rectangle(2, 3, point4);

            Console.WriteLine("\nЗнаходження перетину i об*єднання для таких прямокутникiв:");
            rec1.ShowRectangle();
            rec2.ShowRectangle();
            Console.WriteLine("\nПеретин:");
            try
            {
                Rectangle intersec2 = Rectangle.FindIntersection(rec1, rec2);
                intersec2.ShowRectangle();
            }
            catch (NullReferenceException)
            {
                Console.WriteLine("\nПрямокутники не перетинаються!");
            }

            Console.WriteLine("\nОб*єдняння");
            union = Rectangle.FindUnion(rec1, rec2);
            union.ShowRectangle();

            Console.WriteLine("\nПрямокутник до масштабування:");
            rec1.ShowRectangle();
            Console.WriteLine("\nПрямокутник пiсля масштабування:");
            rec1.Rescale(2);
            rec1.ShowRectangle();

            Console.WriteLine("\nПрямокутник до зсуву вправо:");
            rec1.ShowRectangle();
            Console.WriteLine("\nПрямокутник пiсля зсуву вправо:");
            rec1.Move(0,2);
            rec1.ShowRectangle();

        }
    }
}
