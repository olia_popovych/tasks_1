﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_4
{
    class Program
    {
        static void Main(string[] args)
        {
            Matrix mat = new Matrix(4, 4);
            Matrix mat2 = new Matrix(4, 4);

            mat.FillRandom();
            mat2.FillRandom();

            Console.WriteLine("\nМатриця 1:");
            mat.ShowMatrix();
            Console.WriteLine("\nМатриця 2:");
            mat2.ShowMatrix();

            Matrix mul = Matrix.MultiplyMatrixes(mat, mat2);
            Console.WriteLine("\nМноження матриць:");
            mul.ShowMatrix();

            Matrix sum = Matrix.AddMatrixes(mat, mat2);
            Console.WriteLine("\nДодавання матриць:");
            sum.ShowMatrix();

            Matrix dif = Matrix.SubstractMatrixes(mat, mat2);
            Console.WriteLine("\nВфднiмання матриць:");
            dif.ShowMatrix();

            
            Matrix mat3 = new Matrix(5,5);
            mat3.FillRandom();
            Console.WriteLine("\nМатриця:");
            mat3.ShowMatrix();


            Console.WriteLine("\nМiнор матрицi 2-го порядку на перетинi 1 рядка i 0 стовпця i 2 рядка i 1 стовпця:");
            double minor3 = mat3.MinorKOrder(new int[] { 1, 2 }, new int[] { 0, 1 });
            Console.WriteLine("{0}", minor3.ToString());

            Console.WriteLine("\nДоповнювальний мiнор матрицi для мiнору 2-го порядку на перетинi 1 рядка i 0 стовпця i 2 рядка i 1 стовпця:");
            minor3 = mat3.AdditionalMinorKOrder(new int[] { 1, 2 }, new int[] { 0, 1 });
            Console.WriteLine("{0}", minor3.ToString());

            Console.WriteLine("\nМiнор матрицi для ел-та на перетинi 0 рядка i 0 стовпця:");
            minor3 = Matrix.MinorIJ(mat3, 0, 0);
            Console.WriteLine("{0}", minor3.ToString());

        }
    }
}
